package sgntverifier;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author es03645
 */
 
public class CertificateRevocationException extends Exception {
    private static final long serialVersionUID = 1L;
 
    public CertificateRevocationException(String message, Throwable cause) {
        super(message, cause);
    }
 
    public CertificateRevocationException(String message) {
        super(message);
    }
}