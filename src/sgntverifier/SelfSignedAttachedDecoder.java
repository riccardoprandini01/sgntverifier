package sgntverifier;

//import java.io.ByteArrayInputStream;

import java.io.ByteArrayInputStream;
import org.bouncycastle.util.encoders.Base64;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERUTCTime;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSSignedDataParser;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author es03645
 */
public class SelfSignedAttachedDecoder {

    private String KEYSTOREPATH = "";
    private String KEYSTOREPASSWORD = "";

    SelfSignedAttachedDecoder(String KEYSTOREPATH, String KEYSTOREPASSWORD) {
        this.KEYSTOREPATH = KEYSTOREPATH;
        this.KEYSTOREPASSWORD = KEYSTOREPASSWORD;
    }

    String decode(String envelopedData)  {

        String s = null;
        
        try {

            try {
                Class<?> cBC = Class.forName("org.bouncycastle.jce.provider.BouncyCastleProvider");
                java.security.Security.insertProviderAt((java.security.Provider) cBC.newInstance(), 2000);
                cBC.newInstance();
            } catch (Exception e) {
                //JOptionPane.showMessageDialog(rootPane, e.getMessage(), "Error loading BC", JOptionPane.ERROR_MESSAGE);
            }
            //Security.addProvider(new BouncyCastleProvider());
            CMSSignedDataParser sp=null;
            CMSSignedData sd= null;
            try {
                byte[] sign64 = envelopedData.getBytes();
		byte[] signedData =  Base64.decode(sign64);	
                //sp = new CMSSignedDataParser(signedData);
                //sp = new CMSSignedDataParser(Base64.decode(envelopedData.getBytes()));
                
                
                sd = new CMSSignedData(signedData); // bouncy castle
		//
		
		
                //  signersStore.size());
            } catch (Exception e) {
                //JOptionPane.showMessageDialog(rootPane, e.getMessage(), "Error loading BC", JOptionPane.ERROR_MESSAGE);
            }
            
            //if (sp != null) {
            if (sd != null) {    
                //sp.getSignedContent().drain();
                CertStore certs = sd.getCertificatesAndCRLs("Collection", "BC");
                //CertStore certs = sp.getCertificatesAndCRLs("Collection", "BC");
                //I certificati alla rinfusa
                this.inspectAllCert(certs);
                LinkedList<LinkedList<X509Certificate>> orderedCert = this.orderAllCert(certs);
//------------------------------------------------------------------------------                
                System.out.println("Sono presenti i seguenti certificati nel messaggio");
                for (LinkedList<X509Certificate> linkedList : orderedCert) {
                    System.out.println("Gruppo:");
                    for (X509Certificate x509Certificate : linkedList) {
                        detailCertificate(x509Certificate);
                        System.out.println(""+x509Certificate.hashCode());
                    }
                }
                
                
//https://android.googlesource.com/platform/external/bouncycastle/+/e1142c149e244797ce73b0e7fad40816e447a817/bcpkix/src/main/java/org/bouncycastle/cms/CMSUtils.java
//------------------------------------------------------------------------------                
                
              //  http://stackoverflow.com/questions/13675583/sign-cades-using-bouncycastle-using-java
                
                //http://what-when-how.com/itext-5/digital-signatures-ocsp-and-timestamping-part-2-itext-5/
//------------------------------------------------------------------------------                
                //Verifica sul keystore
                if (KEYSTOREPATH != null && KEYSTOREPASSWORD != null && KEYSTOREPATH.length() > 0 && KEYSTOREPASSWORD.length() > 0) {
                    System.out.println("******************************************************");
                    System.out.println("**************VERIFICA NEL KEYSTORE********************");
                    //Controllo se il certificato è nel keystore 

                    String storename = KEYSTOREPATH;
                    char[] storepass = KEYSTOREPASSWORD.toCharArray();

                    KeyStore ks = null;

                    ks = KeyStore.getInstance("JKS");

                    FileInputStream fin = null;

                    fin = new FileInputStream(storename);
                    ks.load(fin, storepass);
                    
                    
                   

                    
                    System.out.println("**************ANALISI VS KEYSTORE********************");
                    for (LinkedList<X509Certificate> linkedList : orderedCert) {
                        for (X509Certificate x509Certificate : linkedList) {
                            detailCertificate(x509Certificate);
                            if (CertChainValidator.validateKeyChain(x509Certificate, ks)) {
                                System.out.println(">>>>>>>>>>>OK");
                            } else {
                                System.out.println(">>>>>>>>>>>KO");
                            }
                         System.out.println("-C-");
                        }
                        System.out.println("-G-");
                    }
                }
                System.out.println("******************************************************");

//------------------------------------------------------------------------------   
                System.out.println("******************************************************");
                System.out.println("**************ANALISI VS CRL********************");
                for (LinkedList<X509Certificate> linkedList : orderedCert) {
                    for (X509Certificate x509Certificate : linkedList) {
                        detailCertificate(x509Certificate);
                        boolean revoked = false;
                        boolean verified = true;
                        try {
                            java.security.cert.X509Certificate x509CertificateC;
                            x509CertificateC = null;
                            try {
                                byte[] encoded = x509Certificate.getEncoded();
                                ByteArrayInputStream bis = new ByteArrayInputStream(encoded);
                                java.security.cert.CertificateFactory cf = java.security.cert.CertificateFactory.getInstance("X.509");
                                x509CertificateC = (java.security.cert.X509Certificate) cf.generateCertificate(bis);
                            } catch (java.security.cert.CertificateEncodingException e) {
                            } catch (java.security.cert.CertificateException e) {
                            }
                            //CRLVerifier.verifyCertificateOnlineCRLs(x509CertificateC);
                           
                            CRLVerifier.verifyCertificateLocalCRLs(x509CertificateC);
                        } catch (Exception ex) {
                            if(ex instanceof CertificateRevocationException){
                               revoked= true;
                               System.out.println("Certificato REVOCATO: "+ex.getMessage()); 
                            }else{
                            //if(ex instanceof CertificateVerificationException){
                               verified = false;
                               System.out.println("Stato di revoca non verificato: "+ex.getMessage());  
                            }
                        }
                        if(verified && !revoked){
                            System.out.println("Certificato VALIDO"); 
                        }
                    }
                }
                System.out.println("******************************************************");
//------------------------------------------------------------------------------
                System.out.println("************************Correttexzza busta******************************");                
                {

                    String type = "P7M";
                    boolean signed = false;
                    SignerInformationStore signersStore = sd.getSignerInfos();
                    Collection<SignerInformation> signers = signersStore.getSigners();
                    if (signers == null || signers.isEmpty()) {
                        signed = false;
                    } else {
                        //Controllo se l'algoritmo � di tipo SHA256 e che sia presente l'attributo contenente il certificato
                        for (SignerInformation signer : signers) {
                            AttributeTable signedTable = signer.getSignedAttributes();
                            boolean certv2 = signedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_signingCertificateV2);
                            boolean cert = signedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_signingCertificate);
                            if (certv2 && cert) {
                                signed = false;
                                break;
                            } else {
                            // L'algoritmo di digest deve essere SHA256, inoltre deve essere presente il certificato
                                // come attributo signing-certificate oppure ESS-signing-certificate-v2
                                if (!(CMSSignedDataGenerator.DIGEST_SHA256.equals(signer.getDigestAlgOID()) && (certv2 || !cert))) {
                                    signed = false;
                                    break;
                                }
                            }
                            signed = true;

                            // I formati CAdES_T e CAdES_C sono pi� restrittivi di CAdES_BES
                            if (type == null) {
                                type = "CAdES_BES";
                            }
                            //TODO Controllo da verificare
                            AttributeTable unsignedTable = signer.getUnsignedAttributes();
                            if (unsignedTable != null && unsignedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_signatureTimeStampToken)) {
                                type = "CAdES_T";
                                //Controllo se sono presenti gli atttibuti CRL negli attributi unsigned
                                if (unsignedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_ets_certificateRefs) && unsignedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_ets_revocationRefs)) {
                                    type = "CAdES_C";
                                }
                            }

                        }
                    }

                    System.out.println("Type: " + type + " - " + "Signed:" + signed);
                }
//------------------------------------------------------------------------------                               
                System.out.println("************************ESTRAZIONE******************************");                

                //I firmatari
                SignerInformationStore  signers = sd.getSignerInfos();
                 System.out.println("Firmatari:"+signers.size());
                //SignerInformationStore signers = sp.getSignerInfos();
                Collection c = signers.getSigners();
                Iterator it = c.iterator();
                while (it.hasNext()) {
                    System.out.println(">>>>>Firmatario");
                    SignerInformation signer = (SignerInformation) it.next();
                    this.getSigninTime(signer);
                    System.out.println(">>>>>Certificati del firmatario");
                    //Cerco nell'elenco dei certificati per il firmatario
                    Collection certCollection = certs.getCertificates(signer.getSID());
                    Iterator   certIt = certCollection.iterator();
                    
                    //X509Certificate cert = (X509Certificate)certIt.next(); 
                    X509Certificate cert = (X509Certificate)certIt.next(); 
                    
                    detailCertificate(cert);
                    if (signer.verify(cert.getPublicKey(), "BC")){
                        System.out.println("Messaggio verificato  dal presente firmatario");
                    } 
                    
                    
                    
                    
                    
                    
                    
                    AttributeTable unsigned = signer.getUnsignedAttributes();
                    AttributeTable signed = signer.getSignedAttributes();
                    
                    if (unsigned != null) {
                       //TODO come sotto
                    }
                    
                 
                    

                    if (signed != null) {
                        Hashtable<DERObjectIdentifier, Attribute> hts = signed.toHashtable();
                        Set<DERObjectIdentifier> keys = hts.keySet();
                        for (DERObjectIdentifier key : keys) {
                            Attribute attr = hts.get(key);
                            if (attr != null) {
                                ASN1Set set = attr.getAttrValues();
                                if (PKCSObjectIdentifiers.pkcs_9_at_signingTime.equals(key)) {
                                    System.out.println("Orario firma: ");
                                    for (int i = 0; i < set.size(); i++) {
                                        Object object = set.getObjectAt(i);
                                        
                                         if (object instanceof DERUTCTime) {
                                            DERUTCTime derTime = (DERUTCTime) object;
                                            System.out.println("Value of " + key + " is: "+object.getClass()+" - " +  derTime.getAdjustedDate());
                                        }else{
                                            System.out.println("Value of " + key + " is: "+object.getClass()+" - " + object.toString());
                                        }
                                    }
                                } else if (PKCSObjectIdentifiers.pkcs_9_at_messageDigest.equals(key)) {
                                    System.out.println("Message Digest: ");
                                    //System.out.println("Value of " + key + " is: " + set);
                                    for (int i = 0; i < set.size(); i++) {
                                        Object object = set.getObjectAt(i);
                                        System.out.println("Value of " + key + " is: "+object.getClass()+" - " + object.toString());
                                    }
                                }  else if (PKCSObjectIdentifiers.pkcs_9_at_contentType.equals(key)) {
                                    System.out.println("CryptAlgo: ");
                                  
                                    //System.out.println("Value of " + key + " is: " + set);
                                    for (int i = 0; i < set.size(); i++) {
                                        Object object = set.getObjectAt(i);
                                       
                                        System.out.println("Value of " + key + " is: "+object.getClass()+" - " +  this.decodeAlgo((DERObjectIdentifier)object));
                                    }
                                }else if (PKCSObjectIdentifiers.id_aa_signingCertificateV2.equals(key)) {
                                    System.out.println("CMS Advanced Electronic Signatures (CAdES): ");
                                  
                                    //System.out.println("Value of " + key + " is: " + set);
                                    for (int i = 0; i < set.size(); i++) {
                                        Object object = set.getObjectAt(i);
                                       
                                        System.out.println("Value of " + key + " is: "+object.getClass()+" - " +  object.toString());
                                    }
                                }else {
                                    for (int i = 0; i < set.size(); i++) {
                                        Object object = set.getObjectAt(i);
                                        System.out.println("Value of " + key + " is: "+object.getClass()+" - " + object.toString());
                                    }
                                    //System.out.println("Value of " + key + " is: " + set);
                                }

                            }

                        }

                    }
                              
    
                    
                    
                }
                
                
                //ESTRAZIONE MESSAGGIO ORIGINALE
                CMSProcessable signedContent = sd.getSignedContent() ;
		if (signedContent != null) {
		    byte[] originalContent  = (byte[]) signedContent.getContent();
                    s = new String(originalContent);
                }
                
           }

        } catch (CMSException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertStoreException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyStoreException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidAlgorithmParameterException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(SelfSignedAttachedDecoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;

    }

    private void getSigninTime(SignerInformation signer) throws ParseException  {
        Attribute signinTime = signer.getSignedAttributes().get(CMSAttributes.signingTime);
        if (signinTime != null) {
            ASN1Set set = signinTime.getAttrValues();
            for (int i = 0; i < set.size(); i++) {
                Object object = set.getObjectAt(i);
                 System.out.println(""+object.getClass());
//                if (object instanceof ASN1UTCTime) {
//                    ASN1UTCTime asn1Time = (ASN1UTCTime) object;
//                    System.out.println("Firmato il: " + asn1Time.getDate());
//                } else 
                 if (object instanceof DERUTCTime) {
                    DERUTCTime derTime = (DERUTCTime) object;
                    System.out.println("Firmato il: " + derTime.getAdjustedDate());
                }
            }
        }
    }

//    private void risali(X500Name issuer) {
//        System.out.print(issuer);
//    }

    private void detailCertificate(X509Certificate cert) throws CertificateEncodingException {

        System.out.println("Dal:" + cert.getNotBefore());
        System.out.println("Al:" + cert.getNotAfter());
        boolean valid = true;
        try {
            cert.checkValidity();
        } catch (CertificateExpiredException e) {
            System.out.println("Scaduto");
            valid = false;
        } catch (CertificateNotYetValidException e) {
            System.out.println("Non ancora valido");
            valid = false;
        }

        if (valid) {
            System.out.println("Certificato temporalmente valido");
        }

        //X500Principal iss = cert.getIssuerX500Principal();
        //X500Principal sub = cert.getSubjectX500Principal();
        //String issName = iss.getName("RFC2253");
        //String subName = sub.getName("RFC2253");
        //X500Name x500nameIss = new JcaX509CertificateHolder(cert).getIssuer();
        //X500Name x500nameSub = new JcaX509CertificateHolder(cert).getSubject();
        //RDN[] a = x500nameIss.getRDNs();
        //RDN[] b = x500nameSub.getRDNs();
        //RDN cn = x500nameSub.getRDNs(BCStyle.CN)[0];
        //IETFUtils.valueToString(cn.getFirst().getValue());
        //System.out.println("Dati firmatario: " + x500nameSub);
        //System.out.println("Dati Emittente: " + x500nameIss);
        X500Principal certActual = cert.getSubjectX500Principal();
        System.out.println("Attuale:\t " + certActual);

        X500Principal certFirm = cert.getIssuerX500Principal();
        System.out.println("Superiore:\t " + certFirm);

    }

    private void inspectAllCert(CertStore certStore) throws CertificateException, CertStoreException {
        
        Collection<? extends Certificate> certificates = certStore.getCertificates(null);
        Iterator it = certificates.iterator();
        while( it.hasNext()){
            X509Certificate cert = (X509Certificate)it.next();
            //System.out.println(">\t "+cert.getSubjectDN());
        }
    }

    private LinkedList<LinkedList<X509Certificate>> orderAllCert(CertStore certStore) throws CertificateException, NoSuchAlgorithmException, NoSuchProviderException, CertStoreException {
//
        LinkedList<LinkedList<X509Certificate>> listListCert = new LinkedList();

        //recupero tutto con un selettore nullo
        Collection<? extends Certificate> listCertDatFirm = certStore.getCertificates(null);
        Iterator itListCertDatFirm = listCertDatFirm.iterator();
        
       

        //Ciclo 1 per i self signed
        while (itListCertDatFirm.hasNext()) {
            X509Certificate cert = (X509Certificate)itListCertDatFirm.next();
            
            if (SignRelationshipHelper.isSelfSigned(cert)) {
                // lo aggiungo in una nuova lista
                LinkedList<X509Certificate> newListCert = new LinkedList();
                newListCert.add(cert);
                listListCert.add(newListCert);
                //lo rimuovo dalla vecchia
                itListCertDatFirm.remove();
            }
        }

        //Ciclo 2 che itera fino a che ci sono cambiamenti
        boolean changed = true;
        while (changed) {
            changed = false;
            itListCertDatFirm = listCertDatFirm.iterator();
            while (itListCertDatFirm.hasNext()) {
                 X509Certificate cert = (X509Certificate)itListCertDatFirm.next();

                //scorro tutte le liste
                int indexListListCert = 0;
                int indexListCert = 0;
                for (LinkedList<X509Certificate> listCert : listListCert) {
                    for (X509Certificate x509Certificate : listCert) {
                        if (SignRelationshipHelper.isSigneable(cert, x509Certificate)) {
                            //System.out.println(cert);
                            //System.out.println("FIRMATO");
                            //System.out.println(x509Certificate);

                            indexListCert = listCert.indexOf(x509Certificate);
                            indexListListCert = listListCert.indexOf(listCert);
                            changed = true;
                        }
                        if (changed) {
                            break;
                        }
                    }
                    if (changed) {
                        break;
                    }
                }

                if (changed) {
                    LinkedList<X509Certificate> listCert = listListCert.get(indexListListCert);
                    listCert.add(indexListCert, cert);
                    itListCertDatFirm.remove();
                }

            }
        }

        //ciclo 3 i restanti certificati sono...
        return listListCert;
    }

    private String decodeAlgo(DERObjectIdentifier dERObjectIdentifier) {
        HashMap<String,String> algos = new HashMap();
        algos.put("1.2.840.113549",         "szOID_RSA");
        algos.put("1.2.840.113549.1",       "szOID_PKCS ");
        algos.put("1.2.840.113549.2",       "szOID_RSA_HASH ");
        algos.put("1.2.840.113549.3",       "szOID_RSA_ENCRYPT ");
        algos.put("1.2.840.113549.1.1",     "szOID_PKCS_1");
        algos.put("1.2.840.113549.1.2",     "szOID_PKCS_2");
        algos.put("1.2.840.113549.1.3",     "szOID_PKCS_3");
        algos.put("1.2.840.113549.1.4",     "szOID_PKCS_4");
        algos.put("1.2.840.113549.1.5",     "szOID_PKCS_5");
        algos.put("1.2.840.113549.1.6",     "szOID_PKCS_6");
        algos.put("1.2.840.113549.1.7",     "szOID_PKCS_7");
        algos.put("1.2.840.113549.1.8",     "szOID_PKCS_8");
        algos.put("1.2.840.113549.1.9",     "szOID_PKCS_9");
        algos.put("1.2.840.113549.1.10",    "szOID_PKCS_10");
        algos.put("1.2.840.113549.1.12",    "szOID_PKCS_12");
        algos.put("1.2.840.113549.1.1.2",   "szOID_RSA_MD2");
        algos.put("1.2.840.113549.1.1.3",   "szOID_RSA_MD4");
        algos.put("1.2.840.113549.1.1.4",   "szOID_RSA_MD5");
        algos.put("1.2.840.113549.1.1.1",   "szOID_RSA_RSA");
        algos.put("1.2.840.113549.1.1.2",   "szOID_RSA_MD2RSA");
        algos.put("1.2.840.113549.1.1.3",   "szOID_RSA_MD4RSA");
        algos.put("1.2.840.113549.1.1.4",   "szOID_RSA_MD5RSA");
        algos.put("1.2.840.113549.1.1.5",   "szOID_RSA_SHA1RSA");
        algos.put("1.2.840.113549.1.1.5",   "szOID_RSA_SETOAEP_RSA");
        algos.put("1.2.840.113549.1.3.1",   "szOID_RSA_DH");
        algos.put("1.2.840.113549.1.7.1",   "szOID_RSA_data");
        algos.put("1.2.840.113549.1.7.2",   "szOID_RSA_signedData");
        algos.put("1.2.840.113549.1.7.3",   "szOID_RSA_envelopedData");
        algos.put("1.2.840.113549.1.7.4",   "szOID_RSA_signEnvData");
        algos.put("1.2.840.113549.1.7.5",   "szOID_RSA_digestedData");
        algos.put("1.2.840.113549.1.7.5",   "szOID_RSA_hashedData");
        algos.put("1.2.840.113549.1.7.6",   "szOID_RSA_encryptedData");
        algos.put("1.2.840.113549.1.9.1",   "szOID_RSA_emailAddr");
        algos.put("1.2.840.113549.1.9.2",   "szOID_RSA_unstructName");
        algos.put("1.2.840.113549.1.9.3",   "szOID_RSA_contentType");
        algos.put("1.2.840.113549.1.9.4",   "szOID_RSA_messageDigest");
        algos.put("1.2.840.113549.1.9.5",   "szOID_RSA_signingTime");
        algos.put("1.2.840.113549.1.9.6",   "szOID_RSA_counterSign");
        algos.put("1.2.840.113549.1.9.7",   "szOID_RSA_challengePwd");
        algos.put("1.2.840.113549.1.9.8",   "szOID_RSA_unstructAddr");
        algos.put("1.2.840.113549.1.9.9",   "szOID_RSA_extCertAttrs");
        algos.put("1.2.840.113549.1.9.15",  "szOID_RSA_SMIMECapabilities");
        algos.put("1.2.840.113549.1.9.15.1","szOID_RSA_preferSignedData");
        algos.put("1.2.840.113549.3.2",     "szOID_RSA_RC2CBC");
        algos.put("1.2.840.113549.3.4",     "szOID_RSA_RC4");
        algos.put("1.2.840.113549.3.7",     "szOID_RSA_DES_EDE3_CBC");
        algos.put("1.2.840.113549.3.9",     "szOID_RSA_RC5_CBCPad");
        algos.put("1.2.840.10046",          "szOID_ANSI_x942");
        algos.put("1.2.840.10046.2.1",      "szOID_ANSI_x942_DH");
        algos.put("1.2.840.10040",          "szOID_X957");
        algos.put("1.2.840.10040.4.1",      "szOID_X957_DSA");
        algos.put("1.2.840.10040.4.3",      "szOID_DATA STRUCTURE");
        algos.put("2.5",                    "szOId_DS");
        algos.put("2.5.8",                  "szOID_DSALG");
        algos.put("2.5.8.1",                "szOID_DSALG_CRPT");
        algos.put("2.5.8.2",                "szOID_DSALG_HASH");
        algos.put("2.5.8.3",                "szOID_DSALG_SIGN");
        algos.put("2.5.8.1.1",              "szOID_DSALG_RSA");
        algos.put("1.3.14",                 "szOID_OIW");
        algos.put("1.3.14.3.2",             "szOID_OIWSEC");
        algos.put("1.3.14.3.2.2",           "szOID_OIWSEC_md4RSA");
        algos.put("1.3.14.3.2.3",           "szOID_OIWSEC_md5RSA");
        algos.put("1.3.14.3.2.4",           "szOID_OIWSEC_md4RSA2");
        algos.put("1.3.14.3.2.6",           "szOID_OIWSEC_desECB");
        algos.put("1.3.14.3.2.7",           "szOID_OIWSEC_desCBC");
        algos.put("1.3.14.3.2.8",           "szOID_OIWSEC_desOFB");
        algos.put("1.3.14.3.2.9",           "szOID_OIWSEC_desCFB");
        algos.put("1.3.14.3.2.10",          "szOID_OIWSEC_desMAC");
        algos.put("1.3.14.3.2.11",          "szOID_OIWSEC_rsaSign");
        algos.put("1.3.14.3.2.12",          "szOID_OIWSEC_dsa");
        algos.put("1.3.14.3.2.13",          "szOID_OIWSEC_shaDSA");
        algos.put("1.3.14.3.2.14",          "szOID_OIWSEC_mdc2RSA");
        algos.put("1.3.14.3.2.15",          "szOID_OIWSEC_shaRSA");
        algos.put("1.3.14.3.2.16",          "szOID_OIWSEC_dhCommMod");
        algos.put("1.3.14.3.2.17",          "szOID_OIWSEC_desEDE");
        algos.put("1.3.14.3.2.18",          "szOID_OIWSEC_sha");
        algos.put("1.3.14.3.2.19",          "szOID_OIWSEC_mdc2");
        algos.put("1.3.14.3.2.20",          "szOID_OIWSEC_dsaComm");
        algos.put("1.3.14.3.2.21",          "szOID_OIWSEC_dsaCommSHA");
        algos.put("1.3.14.3.2.22",          "szOID_OIWSEC_rsaXchg");
        algos.put("1.3.14.3.2.23",          "szOID_OIWSEC_keyHashSeal");
        algos.put("1.3.14.3.2.24",          "szOID_OIWSEC_md2RSASign");
        algos.put("1.3.14.3.2.25",          "szOID_OIWSEC_md5RSASign");
        algos.put("1.3.14.3.2.26",          "szOID_OIWSEC_sha1");
        algos.put("1.3.14.3.2.27",          "szOID_OIWSEC_dsaSHA1");
        algos.put("1.3.14.3.2.28",          "szOID_OIWSEC_dsaCommSHA1");
        algos.put("1.3.14.3.2.29",          "szOID_OIWSEC_sha1RSASign");
        algos.put("1.3.14.7.2",             "szOID_OIWDIR");
        algos.put("1.3.14.7.2.1",           "szOID_OIWDIR_CRPT");
        algos.put("1.3.14.7.2.2",           "szOID_OIWDIR_HASH");
        algos.put("1.3.14.7.2.3",           "szOID_OIWDIR_SIGN");
        algos.put("1.3.14.7.2.2.1",         "szOID_OIWDIR_md2");
        algos.put("1.3.14.7.2.3.1",         "szOID_OIWDIR_md2RSA");
        algos.put("2.16.840.1.101.2.1",     "szOID_INFOSEC");
        algos.put("2.16.840.1.101.2.1.1.1", "szOID_INFOSEC_sdnsSignature");
        algos.put("2.16.840.1.101.2.1.1.2", "szOID_INFOSEC_mosaicSignature");
        algos.put("2.16.840.1.101.2.1.1.3", "szOID_INFOSEC_sdnsConfidentiality");
        algos.put("2.16.840.1.101.2.1.1.4", "szOID_INFOSEC_mosaicConfidentiality");
        algos.put("2.16.840.1.101.2.1.1.5", "szOID_INFOSEC_sdnsIntegrity");
        algos.put("2.16.840.1.101.2.1.1.6", "szOID_INFOSEC_mosaicIntegrity");
        algos.put("2.16.840.1.101.2.1.1.7", "szOID_INFOSEC_sdnsTokenProtection");
        algos.put("2.16.840.1.101.2.1.1.8", "szOID_INFOSEC_mosaicTokenProtection");
        algos.put("2.16.840.1.101.2.1.1.9", "szOID_INFOSEC_sdnsKeyManagement");
        algos.put("2.16.840.1.101.2.1.1.10","szOID_INFOSEC_mosaicKeyManagement");
        algos.put("2.16.840.1.101.2.1.1.11","szOID_INFOSEC_sdnsKMandSig");
        algos.put("2.16.840.1.101.2.1.1.12","szOID_INFOSEC_mosaicKMandSig");
        algos.put("2.16.840.1.101.2.1.1.13","szOID_INFOSEC_SuiteASignature");
        algos.put("2.16.840.1.101.2.1.1.14","szOID_INFOSEC_SuiteAConfidentiality");
        algos.put("2.16.840.1.101.2.1.1.15","szOID_INFOSEC_SuiteAIntegrity");
        algos.put("2.16.840.1.101.2.1.1.16","szOID_INFOSEC_SuiteATokenProtection");
        algos.put("2.16.840.1.101.2.1.1.17","szOID_INFOSEC_SuiteAKeyManagement");
        algos.put("2.16.840.1.101.2.1.1.18","szOID_INFOSEC_SuiteAKMandSig");
        algos.put("2.16.840.1.101.2.1.1.19","szOID_INFOSEC_mosaicUpdatedSig");
        algos.put("2.16.840.1.101.2.1.1.20","szOID_INFOSEC_mosaicKMandUpdSig");
        algos.put("2.16.840.1.101.2.1.1.21","szOID_INFOSEC_mosaicUpdatedInteg");
        if(algos.containsKey(dERObjectIdentifier.getId())){
            return algos.get(dERObjectIdentifier.getId());
        }else{
            return dERObjectIdentifier.getId();
        }
        
       
        
    }    

}
