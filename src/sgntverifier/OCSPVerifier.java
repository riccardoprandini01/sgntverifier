/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sgntverifier;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.Security;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ocsp.OCSPResponse;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.ocsp.BasicOCSPResp;
import org.bouncycastle.ocsp.CertificateID;
import org.bouncycastle.ocsp.CertificateStatus;
import org.bouncycastle.ocsp.OCSPException;
import org.bouncycastle.ocsp.OCSPReq;
import org.bouncycastle.ocsp.OCSPReqGenerator;
import org.bouncycastle.ocsp.OCSPResp;
import org.bouncycastle.ocsp.OCSPRespStatus;
import org.bouncycastle.ocsp.SingleResp;
import org.bouncycastle.util.encoders.Base64;

/**
 * http://svn.apache.org/repos/asf/synapse/trunk/java/modules/transports/core/nhttp/src/main/java/org/apache/synapse/transport/utils/sslcert/ocsp/OCSPVerifier.java
 * http://svn.apache.org/repos/asf/synapse/trunk/java/modules/transports/core/nhttp/src/main/java/org/apache/synapse/transport/utils/sslcert/ocsp/OCSPCache.java
 * @author es03645
 */
class OCSPVerifier {

    static void verifyCertificateOCSP(X509Certificate x509CertificateC) {
        try {
            RevocationStatus checkRevocationStatus = checkRevocationStatus(x509CertificateC);
        } catch (CertificateVerificationException ex) {
            Logger.getLogger(OCSPVerifier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    
    
    
    /**
     * Gets the revocation status (Good, Revoked or Unknown) of the given peer certificate.
     *
     * @param peerCert   The certificate we want to check if revoked.
     * @param issuerCert Needed to create OCSP request.
     * @return revocation status of the peer certificate.
     * @throws CertificateVerificationException
     *
     * 
     */
    
    
    
    public static RevocationStatus checkRevocationStatus(X509Certificate cert)
            throws CertificateVerificationException {


        //This list will sometimes have non ocsp urls as well.
        List<String> locations = getAIALocations(cert);

        for (String serviceUrl : locations) {

            SingleResp[] responses;
            try {
                //OCSPResp ocspResponse = realizarPeticionYObtenerRespuestaOCSP(serviceUrl, cert);
//                if (OCSPRespStatus.SUCCESSFUL != ocspResponse.getStatus()) {
//                    continue; // Server didn't give the response right.
//                }

//                BasicOCSPResp basicResponse = (BasicOCSPResp) ocspResponse.getResponseObject();
//                responses = (basicResponse == null) ? null : basicResponse.getResponses();
                //todo use the super exception
            } catch (Exception e) {
                continue;
            }

//            if (responses != null && responses.length == 1) {
//                SingleResp resp = responses[0];
//                RevocationStatus status = getRevocationStatus(resp);
////                if (cache != null)
////                    cache.setCacheValue(peerCert.getSerialNumber(), resp, request, serviceUrl);
//                return status;
//            }
        }
        throw new CertificateVerificationException("Cant get Revocation Status from OCSP."+locations);
    }
    
    
    
    
    
    
    
    

    private static RevocationStatus getRevocationStatus(SingleResp resp) throws CertificateVerificationException {
        Object status = resp.getCertStatus();
        if (status == CertificateStatus.GOOD) {
            return RevocationStatus.GOOD;
        } else if (status instanceof org.bouncycastle.ocsp.RevokedStatus) {
            return RevocationStatus.REVOKED;
        } else if (status instanceof org.bouncycastle.ocsp.UnknownStatus) {
            return RevocationStatus.UNKNOWN;
        }
        throw new CertificateVerificationException("Cant recognize Certificate Status");
    }
    
    
    
    
    
    
    
    
    
     /**
     * Authority Information Access (AIA) is a non-critical extension in an X509 Certificate. This contains the
     * URL of the OCSP endpoint if one is available.
     * TODO: This might contain non OCSP urls as well. Handle this.
     *
     * @param cert is the certificate
     * @return a lit of URLs in AIA extension of the certificate which will hopefully contain an OCSP endpoint.
     * @throws CertificateVerificationException
     *
     */
    private static List<String> getAIALocations(X509Certificate cert) throws CertificateVerificationException {

        //Gets the DER-encoded OCTET string for the extension value for Authority information access Points
        byte[] aiaExtensionValue = cert.getExtensionValue(X509Extensions.AuthorityInfoAccess.getId());
        if (aiaExtensionValue == null) {
            throw new CertificateVerificationException("Certificate doesn't have authority " +
                    "information access points");
        }
        //might have to pass an ByteArrayInputStream(aiaExtensionValue)
        ASN1InputStream asn1In = new ASN1InputStream(aiaExtensionValue);
        AuthorityInformationAccess authorityInformationAccess;

        try {
            DEROctetString aiaDEROctetString = (DEROctetString) (asn1In.readObject());
            ASN1InputStream asn1InOctets = new ASN1InputStream(aiaDEROctetString.getOctets());
            ASN1Sequence aiaASN1Sequence = (ASN1Sequence) asn1InOctets.readObject();
            authorityInformationAccess = AuthorityInformationAccess.getInstance(aiaASN1Sequence);
        } catch (IOException e) {
            throw new CertificateVerificationException("Cannot read certificate to get OCSP URLs", e);
        }

        List<String> ocspUrlList = new ArrayList<String>();
        AccessDescription[] accessDescriptions = authorityInformationAccess.getAccessDescriptions();
        for (AccessDescription accessDescription : accessDescriptions) {

            GeneralName gn = accessDescription.getAccessLocation();
            if (gn.getTagNo() == GeneralName.uniformResourceIdentifier) {
                DERIA5String str = DERIA5String.getInstance(gn.getName());
                String accessLocation = str.getString();
                ocspUrlList.add(accessLocation);
            }
        }
        if (ocspUrlList.isEmpty()) {
            throw new CertificateVerificationException("Cant get OCSP urls from certificate");
        }

        return ocspUrlList;
    }
    
    
//    public static OCSPResp sendOCSPRequestPOST(String postURL,X509Certificate certificate) throws Exception  {
//        OCSPResp ocspResp=null;
//        try {
//            byte[] certBase64=Base64.encode(certificate.getEncoded());
//            URL url=new URL(postURL);
//            URLConnection con=url.openConnection();
//            con.setReadTimeout(10000);
//            con.setConnectTimeout(10000);
//            con.setAllowUserInteraction(false);
//            con.setUseCaches(false);
//            con.setDoOutput(true);
//            con.setDoInput(true);
//            con.setRequestProperty("Content-Type","application/ocsp-request");
//            con.setRequestProperty("Accept","application/ocsp-response");
//            OutputStream os=con.getOutputStream();
//            os.write(certBase64);
//            os.close();
//            
//            
//            byte[] resp64=inputStreamToByteArray(con.getInputStream());
//            String s= new String (resp64);
//            byte[] bresp=Base64.decode(resp64);
//            if (bresp.length == 0) {
//                ocspResp=new OCSPResp(new OCSPResponse(null));
//            }else{
//                ocspResp=new OCSPResp(bresp);
//            }
//        }catch (  CertificateEncodingException cee) {
//            throw new Exception("Can not send OCSP request. Certificate encoding is not valid",cee);
//        }catch (  IOException ioe) {
//            throw new Exception("Can not recover response from server " + postURL,ioe);
//  }     return ocspResp;
//}
    

    /** 
 * Envio y recepci?n de la petici?n/respuesta OCSP
 */
public BasicOCSPResp realizarPeticionYObtenerRespuestaOCSP(X509Certificate userCert,X509Certificate caCert,String serviceUrl) throws OCSPException, MalformedURLException, IOException, CertificateException, InterruptedException {
  Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
  OCSPReqGenerator ocspReqGen=new OCSPReqGenerator();
  System.out.println("Nuevo objeto de peticion OCSP creado");
  System.out.println("A?adiendo certificado a verificar");
  CertificateID certid=new CertificateID(CertificateID.HASH_SHA1,caCert,userCert.getSerialNumber());
  ocspReqGen.addRequest(certid);
  System.out.println("Creando Peticion OCSP");
  OCSPReq ocspReq=ocspReqGen.generate();
  URL url=new URL(serviceUrl);
  HttpURLConnection con=(HttpURLConnection)url.openConnection();
  con.setRequestProperty("Content-Type","application/ocsp-request");
  con.setRequestProperty("Accept","application/ocsp-response");
  con.setDoOutput(true);
  OutputStream out=con.getOutputStream();
  DataOutputStream dataOut=new DataOutputStream(new BufferedOutputStream(out));
  System.out.println("Conexion correcta con el servidor OCSP");
  dataOut.write(ocspReq.getEncoded());
  dataOut.flush();
  dataOut.close();
  System.out.println("Peticion OCSP enviada");
  InputStream in=con.getInputStream();
  if (in == null)   throw new IOException("In is null");
  BasicOCSPResp basicResp=(BasicOCSPResp)new OCSPResp(in).getResponseObject();
  System.out.println("Respuesta OCSP recibida");
  con.disconnect();
  out.close();
  in.close();
  return basicResp;
}
    
    

    
}
